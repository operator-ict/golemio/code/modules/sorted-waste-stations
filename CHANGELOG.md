# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.2.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.1.16] - 2022-11-03

### Fixed

-   Apiary docs

## [1.1.15] - 2022-10-11

### Changed

-   Fix up dependencies

## [1.1.14] - 2022-09-21

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.1.13] - 2022-06-07

### Fixed

-   Ksnko notification log ([sorted-waste-stations#14](https://gitlab.com/operator-ict/golemio/code/modules/sorted-waste-stations/-/issues/14))

-   Fixed accessibility param ([sorted-waste-stations#13](https://gitlab.com/operator-ict/golemio/code/modules/sorted-waste-stations/-/issues/13))

## [1.1.12] - 2022-05-25

### Added

-   Ksnko notification log ([sorted-waste-stations#12](https://gitlab.com/operator-ict/golemio/code/modules/sorted-waste-stations/-/issues/12))

## [1.1.11] - 2022-01-07

### Fixed

-   Ksnko source validator ([p0149-chytry-svoz-odpadu#183](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/183))

## [1.1.10] - 2021-11-12

### Added

-   API documentation testing

-   API Endpoint - Pick Days ([p0149-chytry-svoz-odpadu#123](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/123))

### Fixed

-   ID of unknown waste type ([sorted-waste-stations#123](https://gitlab.com/operator-ict/golemio/code/modules/sorted-waste-stations/-/issues/8))
