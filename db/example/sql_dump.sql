-- EXAMPLE DATA - for testing purposes

INSERT INTO public.containers_stations (id, code, knsko_id, accessibility, latitude, longitude, address, district, district_code, "source", last_modify, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
('0a292077-4dbd-501a-ac60-d11c80e889c5', '0001/ 002', 3498, 1, 50.08703701526253, 14.418698026047796, 'Linhartská 136/6', 'praha-1', 500054, 'ksnko', NULL, -1, '2020-11-23 12:45:34.807', 'integration-engine', -1, '2021-03-02 07:37:12.762', 'integration-engine');

INSERT INTO public.containers_containers
(id, code, knsko_id, knsko_code, station_code, total_volume, prediction, bin_type, installed_at, network, cleaning_frequency_interval, cleaning_frequency_frequency, company, container_type, trash_type, "source", create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, sensor_id, pick_days, sensoneo_code)
VALUES('26d55c0e-b433-5d35-ac32-c7d338b711a9', 'S0001002TKOV2150SV15292', 15292, 'S0001002TKOV2150SV15292', '0001/ 002', NULL, NULL, NULL, NULL, NULL, 4, 1, NULL, '2150 L Reflex - SV', 3, 'ksnko', -1, '2021-02-15 08:54:01.561', 'integration-engine', -1, '2021-06-08 07:39:24.118', 'integration-engine', NULL, '5', '0001/ 002C01583');
INSERT INTO public.containers_containers
(id, code, knsko_id, knsko_code, station_code, total_volume, prediction, bin_type, installed_at, network, cleaning_frequency_interval, cleaning_frequency_frequency, company, container_type, trash_type, "source", create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, sensor_id, pick_days, sensoneo_code)
VALUES('acae5106-a213-5dfb-8224-97418cf87fdb', 'S0001002TNKV2150SV15293', 15293, 'S0001002TNKV2150SV15293', '0001/ 002', NULL, NULL, NULL, NULL, NULL, 2, 1, NULL, '2150 L Reflex - SV', 4, 'ksnko', -1, '2021-01-24 07:35:28.330', 'integration-engine', -1, '2021-06-08 07:39:24.118', 'integration-engine', NULL, '4', '0001/ 002C01584');
INSERT INTO public.containers_containers
(id, code, knsko_id, knsko_code, station_code, total_volume, prediction, bin_type, installed_at, network, cleaning_frequency_interval, cleaning_frequency_frequency, company, container_type, trash_type, "source", create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, sensor_id, pick_days, sensoneo_code)
VALUES('31094cd8-e025-5a78-8d39-fa1b1b4c045f', 'S0001002TPV2150SV15294', 15294, 'S0001002TPV2150SV15294', '0001/ 002', NULL, NULL, NULL, NULL, NULL, 1, 4, NULL, '2150 L Reflex - SV', 5, 'ksnko', -1, '2021-01-24 07:35:28.330', 'integration-engine', -1, '2021-06-08 07:39:24.118', 'integration-engine', NULL, '1,2,4,5', '0001/ 002C01582');

INSERT INTO public.containers_picks (container_id, container_code, station_code, pick_at, pick_at_utc, percent_before, percent_now, event_driven, decrease, pick_minfilllevel, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
('7089c1ba-c2a4-52e9-bd54-9448bfdd7afe', '0001/ 002C01583', '0001/ 002', '2021-01-19 13:48:24.000', '2021-01-19 13:48:24.000', 98, 0, false, 20, 30, NULL, '2021-01-20 08:15:12.040', NULL, NULL, '2021-01-26 08:15:48.314', NULL),
('7089c1ba-c2a4-52e9-bd54-9448bfdd7afe', '0001/ 002C01583', '0001/ 002', '2021-01-22 22:00:55.000', '2021-01-22 22:00:55.000', 21, 0, false, 20, 30, NULL, '2021-01-23 08:15:08.301', NULL, NULL, '2021-01-23 08:15:08.301', NULL),
('7089c1ba-c2a4-52e9-bd54-9448bfdd7afe', '0001/ 002C01583', '0001/ 002', '2021-01-24 21:15:43.000', '2021-01-24 21:15:43.000', 26, 0, false, 20, 30, NULL, '2021-01-25 08:15:16.631', NULL, NULL, '2021-01-25 08:15:16.631', NULL),
('7089c1ba-c2a4-52e9-bd54-9448bfdd7afe', '0001/ 002C01583', '0001/ 002', '2021-02-02 13:22:05.000', '2021-02-02 13:22:05.000', 49, 0, false, 20, 30, NULL, '2021-02-03 08:15:10.177', NULL, NULL, '2021-02-09 08:16:47.952', NULL),
('7089c1ba-c2a4-52e9-bd54-9448bfdd7afe', '0001/ 002C01583', '0001/ 002', '2021-02-04 07:28:38.000', '2021-02-04 07:28:38.000', 30, 1, false, 20, 30, NULL, '2021-02-05 08:15:08.949', NULL, NULL, '2021-02-05 08:15:08.949', NULL);

INSERT INTO public.containers_measurement (container_id, container_code, station_code, percent_calculated, upturned, temperature, battery_status, measured_at, measured_at_utc, prediction, prediction_utc, firealarm, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
('7089c1ba-c2a4-52e9-bd54-9448bfdd7afe', '0001/ 002C01583', '0001/ 002', 73, 0, 3, 3.5, '2021-02-02 07:21:08.000', '2021-02-02 07:21:08.000', NULL, NULL, 0, NULL, '2021-02-03 08:09:47.464', NULL, NULL, '2021-02-08 09:32:48.187', NULL),
('7089c1ba-c2a4-52e9-bd54-9448bfdd7afe', '0001/ 002C01583', '0001/ 002', 98, 0, 0, 3.4, '2021-02-08 07:17:23.000', '2021-02-08 07:17:23.000', NULL, NULL, 0, NULL, '2021-02-08 08:22:10.290', NULL, NULL, '2021-02-09 08:17:06.562', NULL);

INSERT INTO public.containers_picks_dates (container_id, pick_date, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES
('7089c1ba-c2a4-52e9-bd54-9448bfdd7afe', '2121-05-06 14:00:00.000', -1, '2021-03-03 12:23:58.635', 'integration-engine', NULL, NULL, NULL);
