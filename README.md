<div align="center">
<p>
    <a href="https://operatorict.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/oict_logo.png" alt="oict" width="100px" height="100px" />
    </a>
    <a href="https://golemio.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/golemio_logo.png" alt="golemio" width="100px" height="100px" />
    </a>
</p>

<h1>@golemio/sorted-waste-stations</h1>

### [DEPRECATED]

:warning: This package has been deprecated. Use [waste-collection](https://gitlab.com/operator-ict/golemio/code/modules/waste-collection) module instead. :warning:

<p>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/sorted-waste-stations/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/sorted-waste-stations/badges/master/pipeline.svg" alt="pipeline">
    </a>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/sorted-waste-stations/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/sorted-waste-stations/badges/master/coverage.svg" alt="coverage">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/npm/l/@golemio/sorted-waste-stations" alt="license">
    </a>
</p>

<p>
    <a href="#installation">Installation</a> · <a href="./docs">Documentation</a> · <a href="https://operator-ict.gitlab.io/golemio/code/modules/sorted-waste-stations">TypeDoc</a>
</p>
</div>

This module is intended for use with Golemio services. Refer [here](https://gitlab.com/operator-ict/golemio/code/modules/core/-/blob/development/README.md) for further information on usage, local development and more.

## Installation

The APIs may be unstable. Therefore, we recommend to install this module as an exact version.

```bash
# Latest version
npm install --save-exact @golemio/sorted-waste-stations@latest

# Development version
npm install --save-exact @golemio/sorted-waste-stations@dev
```

<!-- ## Description -->

## Methods updateSensorsMeasurement and updateSensorsPicks

By default, these methods download data from the source for the last 168 hours (24x7). Data of new measurements are stored in the corresponding tables (`containers_measurement`, `containers_picks`), data of measurements which are already saved in a database - are updated.

It is possible to send a custom AMQP message to the worker for these methods, indicating the desired time range for which you want to update the data in the database.

The format of the message content should be as follows:

```json
{
    "from": "Sun Mar 21 2021 00:19:52 GMT+0100 (GMT+01:00)",
    "to": "Tue Mar 30 2021 00:19:52 GMT+0200 (GMT+02:00)"
}
```

Any `new Date(value)` parsable format.
