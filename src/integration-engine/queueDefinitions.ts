import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { SortedWasteStations } from "#sch/index";
import { SortedWasteStationsWorker } from "#ie/SortedWasteStationsWorker";
import { SortedWasteStationsWorkerPg } from "#ie/SortedWasteStationsWorkerPg";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: `${SortedWasteStations.name}Pg`,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + SortedWasteStations.name.toLowerCase() + "Pg",
        queues: [
            {
                name: "updateSensorsMeasurementPg",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SortedWasteStationsWorkerPg,
                workerMethod: "updateSensorsMeasurement",
            },
            {
                name: "updateSensorsPicksPg",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SortedWasteStationsWorkerPg,
                workerMethod: "updateSensorsPicks",
            },
            {
                name: "updateStationsAndContainersPg",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SortedWasteStationsWorkerPg,
                workerMethod: "updateStationsAndContainers",
            },
            {
                name: "updateSortedWastePicksPg",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SortedWasteStationsWorkerPg,
                workerMethod: "updateSortedWastePicks",
            },
            {
                name: "updateMeasurementsForMonth",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SortedWasteStationsWorkerPg,
                workerMethod: "updateMeasurementsForMonth",
            },
            {
                name: "updateMeasurementsForPrevTwoMonths",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SortedWasteStationsWorkerPg,
                workerMethod: "updateMeasurementsForPrevTwoMonths",
            },
        ],
    },
    {
        name: SortedWasteStations.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + SortedWasteStations.name.toLowerCase(),
        queues: [
            {
                name: "refreshDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SortedWasteStationsWorker,
                workerMethod: "refreshDataInDB",
            },
            {
                name: "updateDistrict",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SortedWasteStationsWorker,
                workerMethod: "updateDistrict",
            },
            {
                name: "getSensorsAndPairThemWithContainers",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: SortedWasteStationsWorker,
                workerMethod: "getSensorsAndPairThemWithContainers",
            },
            {
                name: "updateSensorsMeasurement",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 29 * 60 * 1000, // 29 minutes
                },
                worker: SortedWasteStationsWorker,
                workerMethod: "updateSensorsMeasurement",
            },
            {
                name: "updateSensorsMeasurementInContainer",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 29 * 60 * 1000, // 29 minutes
                },
                worker: SortedWasteStationsWorker,
                workerMethod: "updateSensorsMeasurementInContainer",
            },
            {
                name: "updateSensorsPicks",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 29 * 60 * 1000, // 29 minutes
                },
                worker: SortedWasteStationsWorker,
                workerMethod: "updateSensorsPicks",
            },
            {
                name: "updateSensorsPicksInContainer",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 29 * 60 * 1000, // 29 minutes
                },
                worker: SortedWasteStationsWorker,
                workerMethod: "updateSensorsPicksInContainer",
            },
        ],
    },
];

export { queueDefinitions };
