import axios, { AxiosRequestConfig } from "@golemio/core/dist/shared/axios";
import { SortedWasteStations } from "#sch";
import { PostgresModel, ISequelizeSettings } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { config } from "@golemio/core/dist/integration-engine/config";
import { log } from "@golemio/core/dist/integration-engine/helpers";

interface IKsnkoNotification {
    id?: number;
    knsko_id?: number;
    sensor_id?: number;
    mode?: string;
    success?: boolean;
    request?: string;
    response?: string;
}

const notificationModelConfig = (savingType: ISequelizeSettings["savingType"]) => {
    return new PostgresModel(
        SortedWasteStations.containersKsnkoNotifications.name + "Model",
        {
            outputSequelizeAttributes: SortedWasteStations.containersKsnkoNotifications.outputSequelizeAttributes,
            pgTableName: SortedWasteStations.containersKsnkoNotifications.pgTableName,
            savingType,
        },
        new JSONSchemaValidator(
            SortedWasteStations.containersKsnkoNotifications.name + "ModelValidatorPg",
            SortedWasteStations.containersKsnkoNotifications.outputJSONSchema
        )
    );
};

export class KsnkoNotifier {
    private ksnkoNotificationsInsertModel: PostgresModel;
    private ksnkoNotificationsUpdateModel: PostgresModel;
    private knskoToken!: string;

    constructor() {
        this.ksnkoNotificationsInsertModel = notificationModelConfig("insertOnly");
        this.ksnkoNotificationsUpdateModel = notificationModelConfig("insertOrUpdate");
    }

    public notifyChangedSensors = async (
        containers: {
            attached: Array<{
                knsko_id: number;
                sensor_id: number;
            }>;
            detached: Array<{
                knsko_id: number;
                sensor_id: number;
            }>;
        },
        knskoToken: string
    ): Promise<void> => {
        this.knskoToken = knskoToken;

        for (const container of containers.detached) {
            await this.notifyChangedOneSensor(container, "detached");
        }

        for (const container of containers.attached) {
            await this.notifyChangedOneSensor(container, "attached");
        }
    };

    public notifyChangedOneSensor = async (
        container: {
            knsko_id: number;
            sensor_id: number;
        },
        mode: "detached" | "attached"
    ): Promise<void> => {
        const reqConfig = {
            data: {
                sensorId: container.sensor_id,
            },
            headers: {
                Authorization: `Bearer ***`,
                "Content-Type": "application/json",
                accept: "application/json",
            },
            method: "POST",
            url: `${config.datasources.KSNKOApi.url}/station-item-containers/${container.knsko_id}/sensor/${
                mode === "detached" ? "unassign" : "assign"
            }`,
        };

        const requestObjToDb: IKsnkoNotification = {
            request: JSON.stringify(reqConfig),
            mode: mode === "detached" ? "unassign" : "assign",
        };

        if (container.knsko_id) requestObjToDb.knsko_id = +container.knsko_id;
        if (container.sensor_id) requestObjToDb.sensor_id = +container.sensor_id;

        const savedRequest = await this.insertNotificationDataToDb(requestObjToDb);

        const id = savedRequest.id;

        try {
            reqConfig.headers.Authorization = `Bearer ${this.knskoToken}`;
            const responseFromKsnko = await axios.request(reqConfig as AxiosRequestConfig);

            const responseObjToDb: IKsnkoNotification = {
                id,
                success: true,
                response: JSON.stringify({
                    status: responseFromKsnko.status,
                    statusText: responseFromKsnko.statusText,
                    data: responseFromKsnko.data,
                }),
            };

            await this.updateNotificationDataInDb(responseObjToDb);
        } catch (error) {
            const responseObjToDb: IKsnkoNotification = {
                id,
                success: false,
                response: JSON.stringify({ error: error.message }),
            };

            if (error.isAxiosError) {
                responseObjToDb.response = JSON.stringify({
                    status: error.response.status,
                    statusText: error.response.statusText,
                    headers: error.response.headers,
                    data: error.response.data,
                });
            }
            await this.updateNotificationDataInDb(responseObjToDb);
        }
    };

    private insertNotificationDataToDb = async (notificationDataToDb: IKsnkoNotification) => {
        try {
            return await this.ksnkoNotificationsInsertModel.save(notificationDataToDb);
        } catch (error) {
            log.error("Can not save notification request to DB", error);
        }
    };

    private updateNotificationDataInDb = async (notificationDataToDb: IKsnkoNotification) => {
        try {
            return await this.ksnkoNotificationsUpdateModel.save(notificationDataToDb);
        } catch (error) {
            log.error("Can not save notification response to DB", error);
        }
    };
}
