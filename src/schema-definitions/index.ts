import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";
import Sequelize from "@golemio/core/dist/shared/sequelize";

// MSO = Mongoose SchemaObject
// SDMA = Sequelize DefineModelAttributes

/* tslint:disable:object-literal-sort-keys */
const iprContainersDatasourceMSO: SchemaDefinition = {
    geometry: {
        coordinates: { type: Array, required: true },
        type: { type: String, required: true },
    },
    properties: {
        CLEANINGFREQUENCYCODE: { type: Number, required: true },
        CONTAINERTYPE: { type: String },
        OBJECTID: { type: Number, required: true },
        STATIONID: { type: Number, required: true },
        TRASHTYPENAME: { type: String, required: true },
    },
    type: { type: String, required: true },
};

const iprStationsDatasourceMSO: SchemaDefinition = {
    geometry: {
        coordinates: { type: Array, required: true },
        type: { type: String, required: true },
    },
    properties: {
        CITYDISTRICT: { type: String, required: true },
        CITYDISTRICTRUIANCODE: { type: Number, required: true },
        ID: { type: Number, required: true },
        OBJECTID: { type: Number, required: true },
        PRISTUP: { type: String },
        STATIONNAME: { type: String, required: true },
        STATIONNUMBER: { type: String, required: true },
    },
    type: { type: String, required: true },
};

const oictDatasourceMSO: SchemaDefinition = {
    accessibility: { type: String },
    address: { type: String },
    cleaning_frequency: { type: String },
    company: {
        email: { type: String },
        name: { type: String },
        phone: { type: String },
        web: { type: String },
    },
    coordinates: { type: Array, required: true },
    description: { type: String },
    district: { type: String },
    slug: { type: String, required: true },
    trash_type: { type: String, required: true },
    unique_id: { type: String, required: true },
};

const potexDatasourceMSO: SchemaDefinition = {
    address: { type: String },
    city: { type: String },
    lat: { type: String, required: true },
    lng: { type: String, required: true },
    title: { type: String, required: true },
};

const sensoneoContainersDatasourceMSO: SchemaDefinition = {
    address: { type: String },
    bin_type: { type: String },
    code: { type: String, required: true },
    district: { type: String },
    id: { type: Number, required: true },
    installed_at: { type: String },
    latitude: { type: Number, required: true },
    longitude: { type: Number, required: true },
    postal_code: { type: String },
    prediction: { type: String },
    total_volume: { type: Number },
    trash_type: { type: String, required: true },
};

const sensoneoMeasurementsDatasourceMSO: SchemaDefinition = {
    battery_status: { type: Number },
    code: { type: String, required: true },
    container_id: { type: Number, required: true },
    firealarm: { type: Number },
    id: { type: Number, required: true },
    measured_at: { type: String },
    measured_at_utc: { type: String, required: true },
    percent_calculated: { type: Number, required: true },
    prediction: { type: String },
    prediction_utc: { type: String },
    temperature: { type: Number },
    upturned: { type: Number },
};

const sensoneoPicksDatasourceMSO: SchemaDefinition = {
    code: { type: String, required: true },
    container_id: { type: Number, required: true },
    decrease: { type: Number },
    event_driven: { type: Boolean },
    id: { type: Number, required: true },
    percent_before: { type: Number },
    percent_now: { type: Number },
    pick_at: { type: String },
    pick_at_utc: { type: String, required: true },
    pick_minfilllevel: { type: Number },
};

const outputMSO: SchemaDefinition = {
    geometry: {
        coordinates: { type: Array },
        type: { type: String },
    },
    properties: {
        accessibility: {
            description: { type: String, required: true },
            id: { type: Number, required: true },
        },
        containers: [
            {
                _id: false,
                cleaning_frequency: {
                    duration: { type: String },
                    frequency: { type: Number },
                    id: { type: Number },
                },
                company: {
                    email: { type: String },
                    name: { type: String },
                    phone: { type: String },
                    web: { type: String },
                },
                container_type: { type: String },
                description: { type: String },
                last_measurement: {
                    measured_at_utc: { type: Number },
                    percent_calculated: { type: Number },
                    prediction_utc: { type: Number },
                },
                last_pick: {
                    pick_at_utc: { type: Number },
                },
                sensor_code: { type: String },
                sensor_container_id: { type: Number },
                sensor_supplier: { type: String },
                trash_type: {
                    description: { type: String, required: true },
                    id: { type: Number, required: true },
                },
            },
        ],
        district: { type: String },
        id: { type: Number, required: true },
        name: { type: String, required: true },
        station_number: { type: String },
        updated_at: { type: Number, required: true },
    },
    type: { type: String, required: true },
};

const measurementsOutputMSO: SchemaDefinition = {
    battery_status: { type: Number },
    code: { type: String, required: true },
    container_id: { type: Number, required: true },
    firealarm: { type: Number },
    id: { type: Number, required: true },
    measured_at_utc: { type: Number, required: true },
    percent_calculated: { type: Number, required: true },
    prediction_utc: { type: Number },
    temperature: { type: Number },
    updated_at: { type: Number, required: true },
    upturned: { type: Number },
};

const picksOutputMSO: SchemaDefinition = {
    code: { type: String, required: true },
    container_id: { type: Number, required: true },
    decrease: { type: Number },
    event_driven: { type: Boolean },
    percent_before: { type: Number },
    percent_now: { type: Number },
    pick_at_utc: { type: Number, required: true },
    pick_minfilllevel: { type: Number },
    updated_at: { type: Number, required: true },
};

const outputContainersPicksSDMA: Sequelize.ModelAttributes<any> = {
    container_id: { type: Sequelize.STRING(50) },
    container_code: {
        type: Sequelize.STRING(50),
        primaryKey: true,
    },
    station_code: Sequelize.STRING(50),
    pick_at: { type: Sequelize.DATE },
    pick_at_utc: {
        type: Sequelize.DATE,
        primaryKey: true,
    },
    percent_before: { type: Sequelize.INTEGER },
    percent_now: { type: Sequelize.INTEGER },
    event_driven: Sequelize.BOOLEAN,
    pick_minfilllevel: { type: Sequelize.INTEGER },
    decrease: { type: Sequelize.INTEGER },
    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const picksOutputPgMSO: SchemaDefinition = {
    container_code: { type: String },
    percent_before: { type: Number },
    percent_now: { type: Number },
    event_driven: { type: Boolean },
    decrease: { type: Number },
    pick_at: { type: String },
    pick_at_utc: { type: String },
    pick_minfilllevel: { type: Number },
};

const outputContainersSDMA: Sequelize.ModelAttributes<any> = {
    id: { type: Sequelize.STRING(50) },
    code: {
        type: Sequelize.STRING(50),
        primaryKey: true,
    },
    knsko_id: { type: Sequelize.INTEGER },
    knsko_code: { type: Sequelize.STRING(50) },
    station_code: { type: Sequelize.STRING(50) },
    sensoneo_code: { type: Sequelize.STRING(50) },
    total_volume: { type: Sequelize.INTEGER },
    prediction: { type: Sequelize.DATE },
    bin_type: { type: Sequelize.STRING(150) },
    installed_at: { type: Sequelize.DATE },
    network: { type: Sequelize.STRING(50) },
    cleaning_frequency_interval: { type: Sequelize.INTEGER },
    cleaning_frequency_frequency: { type: Sequelize.INTEGER },
    company: { type: Sequelize.STRING },
    container_type: { type: Sequelize.STRING },
    trash_type: { type: Sequelize.INTEGER },
    source: { type: Sequelize.STRING(10) },
    sensor_id: { type: Sequelize.BIGINT },
    pick_days: { type: Sequelize.STRING(100) },
    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const outputContainersOutputMSO: SchemaDefinition = {
    station_code: { type: String, required: true },
    code: { type: String, required: true },
    total_volume: { type: Number },
    trash_type: { type: String },
    prediction: { type: String },
    bin_type: { type: String },
    installed_at: { type: String },
    network: { type: String },
    cleaning_frequency_interval: { type: Number },
    cleaning_frequency_frequency: { type: Number },
    company: { type: String },
    container_type: { type: String },
    source: { type: String },
    sensor_id: { type: Number },
    pick_days: { type: String },
    sensoneo_code: { type: String },
};

const outputMeasurementSDMA: Sequelize.ModelAttributes<any> = {
    container_id: { type: Sequelize.STRING(50) },
    container_code: {
        type: Sequelize.STRING(50),
        primaryKey: true,
    },
    station_code: Sequelize.STRING(50),
    percent_calculated: { type: Sequelize.INTEGER },
    upturned: { type: Sequelize.INTEGER },
    temperature: { type: Sequelize.INTEGER },
    battery_status: Sequelize.DECIMAL,
    measured_at: { type: Sequelize.DATE },
    measured_at_utc: {
        type: Sequelize.DATE,
        primaryKey: true,
    },
    prediction: { type: Sequelize.DATE },
    prediction_utc: { type: Sequelize.DATE },
    firealarm: { type: Sequelize.INTEGER },
    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const measurementsOutputPgMSO: SchemaDefinition = {
    container_code: { type: String },
    percent_calculated: { type: Number },
    upturned: { type: Number },
    temperature: { type: Number },
    battery_status: { type: Number },
    measured_at: { type: String },
    measured_at_utc: { type: String },
    prediction: { type: String },
    prediction_utc: { type: String },
    firealarm: { type: String },
};

const outputStationsSDMA: Sequelize.ModelAttributes<any> = {
    id: { type: Sequelize.STRING(50) },
    code: {
        type: Sequelize.STRING(20),
        primaryKey: true,
    },
    knsko_id: { type: Sequelize.INTEGER },
    latitude: Sequelize.DECIMAL,
    longitude: Sequelize.DECIMAL,
    address: { type: Sequelize.STRING(250) },
    district: { type: Sequelize.STRING(50) },
    district_code: { type: Sequelize.INTEGER },
    accessibility: { type: Sequelize.INTEGER },
    source: { type: Sequelize.STRING(10) },
    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const pickDatestsOutputPgMSO: SchemaDefinition = {
    container_id: { type: String },
    pick_date: { type: String },
};

/* tslint:disable:object-literal-sort-keys object-literal-key-quotes trailing-comma*/
const pickDatestsOutputJSONSchema = {
    type: "array",
    items: [
        {
            type: "object",
            properties: {
                container_id: {
                    type: "string",
                },
                pick_date: {
                    type: "string",
                },
            },
            required: ["container_id", "pick_date"],
        },
    ],
};
const outputPickDatesSDMA: Sequelize.ModelAttributes<any> = {
    container_id: {
        type: Sequelize.STRING(50),
        primaryKey: true,
    },
    pick_date: {
        type: Sequelize.DATE,
        primaryKey: true,
    },
    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const outputStationsMSO: SchemaDefinition = {
    code: { type: String },
    latitude: { type: Number },
    longitude: { type: Number },
    address: { type: String },
    district: { type: String },
    postal_code: { type: String },
    opening_blocked: { type: String },
    percent_calculated: { type: Number },
};

/* tslint:disable:object-literal-sort-keys object-literal-key-quotes trailing-comma*/

const ksnkoStationsJSONSchema = {
    type: "array",
    items: [
        {
            type: "object",
            properties: {
                id: {
                    type: "integer",
                },
                number: {
                    type: "string",
                },
                name: {
                    type: "string",
                },
                access: {
                    type: "string",
                },
                location: {
                    type: "string",
                },
                cityDistrict: {
                    type: "object",
                    properties: {
                        id: {
                            type: "integer",
                        },
                        name: {
                            type: "string",
                        },
                        ruianCode: {
                            type: "string",
                        },
                    },
                    required: ["id", "name", "ruianCode"],
                },
                coordinate: {
                    type: "object",
                    properties: {
                        lat: {
                            type: "number",
                        },
                        lon: {
                            type: "number",
                        },
                    },
                    required: ["lat", "lon"],
                },
                containers: {
                    type: "array",
                    items: [
                        {
                            type: "object",
                            properties: {
                                id: {
                                    type: "integer",
                                },
                                code: {
                                    type: "string",
                                },
                                sensorId: {
                                    anyOf: [{ type: "string" }, { type: "null" }],
                                },
                                trashType: {
                                    type: "object",
                                    properties: {
                                        code: {
                                            type: "string",
                                        },
                                        name: {
                                            type: "string",
                                        },
                                    },
                                    required: ["code", "name"],
                                },
                            },
                            required: ["id", "code", "sensorId", "trashType"],
                        },
                    ],
                },
            },
            required: ["id", "number", "name", "access", "location", "cityDistrict", "coordinate", "containers"],
        },
    ],
};

const ksnkoNotificationsOutputJSONSchema = {
    type: "object",
    properties: {
        id: {
            type: "number",
        },
        knsko_id: {
            type: "integer",
        },
        sensor_id: {
            type: "integer",
        },
        mode: {
            type: "string",
        },
        success: {
            type: "boolean",
        },
        request: {
            type: "string",
        },
        response: {
            type: "string",
        },
    },
};

export const outputKsnkoNotificationsSDMA: Sequelize.ModelAttributes = {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER,
    },
    knsko_id: { type: Sequelize.INTEGER },
    sensor_id: { type: Sequelize.INTEGER },
    mode: { type: Sequelize.STRING(50) },
    success: { type: Sequelize.BOOLEAN },
    request: { type: Sequelize.JSON },
    response: { type: Sequelize.JSON },

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

const forExport = {
    ipr: {
        datasourceContainersMongooseSchemaObject: iprContainersDatasourceMSO,
        datasourceStationsMongooseSchemaObject: iprStationsDatasourceMSO,
        name: "IPRSortedWasteStations",
    },
    ksnko: {
        ksnkoStationsJSONSchema,
        name: "KSNKOSortedWasteStations",
    },
    mongoCollectionName: "sortedwastestations",
    name: "SortedWasteStations",
    oict: {
        datasourceMongooseSchemaObject: oictDatasourceMSO,
        name: "OICTSortedWasteStations",
    },
    outputMongooseSchemaObject: outputMSO,
    potex: {
        datasourceMongooseSchemaObject: potexDatasourceMSO,
        name: "POTEXSortedWasteStations",
    },
    sensorsContainers: {
        datasourceMongooseSchemaObject: sensoneoContainersDatasourceMSO,
        name: "SortedWasteStationsContainersSensors",
        outputMongooseSchemaObject: outputContainersOutputMSO,
        outputSequelizeAttributes: outputContainersSDMA,
        pgTableName: "containers_containers",
    },
    sensorsMeasurements: {
        datasourceMongooseSchemaObject: sensoneoMeasurementsDatasourceMSO,
        mongoCollectionName: "sortedwastestations_measurements",
        name: "SortedWasteStationsMeasurementsSensors",
        outputMongooseSchemaObject: measurementsOutputMSO,
        outputMongoosePgSchemaObject: measurementsOutputPgMSO,
        outputSequelizeAttributes: outputMeasurementSDMA,
        pgTableName: "containers_measurement",
    },
    sensorsPicks: {
        datasourceMongooseSchemaObject: sensoneoPicksDatasourceMSO,
        mongoCollectionName: "sortedwastestations_picks",
        name: "SortedWasteStationsPicksSensors",
        outputMongooseSchemaObject: picksOutputMSO,
        outputSequelizeAttributes: outputContainersPicksSDMA,
        outputMongoosePgSchemaObject: picksOutputPgMSO,
        pgTableName: "containers_picks",
    },
    stations: {
        name: "SortedWasteStationsStations",
        outputMongooseSchemaObject: outputStationsMSO,
        outputSequelizeAttributes: outputStationsSDMA,
        pgTableName: "containers_stations",
    },
    containersPickDates: {
        name: "SortedWasteStationsContainersPickDates",
        outputPickDatestsOutputJSONSchema: pickDatestsOutputJSONSchema,
        outputSequelizeAttributes: outputPickDatesSDMA,
        pgTableName: "containers_picks_dates",
    },
    containersKsnkoNotifications: {
        name: "SortedWasteStationsContainersKsnkoNotifications",
        outputJSONSchema: ksnkoNotificationsOutputJSONSchema,
        outputSequelizeAttributes: outputKsnkoNotificationsSDMA,
        pgTableName: "containers_ksnko_notifications",
    },
};

export { forExport as SortedWasteStations };
