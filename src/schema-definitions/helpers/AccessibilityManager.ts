export default class AccessibilityManager {
    private static defaultId = "3";
    private static options: Map<string, string> = new Map([
        ["1", "volně"],
        ["2", "obyvatelům domu"],
        ["3", "neznámá dostupnost"],
    ]);

    public static getDescriptionById(id: string): string {
        return this.options.get(id.toString()) ?? this.options.get(this.defaultId)!;
    }

    public static getIdByDescription(text: string): string {
        for (const iterator of this.options) {
            if (iterator[1] === text) return iterator[0];
        }

        return this.defaultId;
    }
}
