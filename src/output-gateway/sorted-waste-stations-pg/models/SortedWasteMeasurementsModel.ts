import Sequelize from "@golemio/core/dist/shared/sequelize";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { SortedWasteStations } from "#sch/index";
import { sequelizeConnection } from "@golemio/core/dist/output-gateway/database";
import getUuid from "uuid-by-string";

export class SortedWasteMeasurementsModel extends SequelizeModel {
    public constructor() {
        super(
            SortedWasteStations.sensorsMeasurements.name,
            SortedWasteStations.sensorsMeasurements.pgTableName,
            SortedWasteStations.sensorsMeasurements.outputSequelizeAttributes
        );

        this.sequelizeModel.removeAttribute("vendor_id");
        this.sequelizeModel.removeAttribute("update_batch_id");
        this.sequelizeModel.removeAttribute("create_batch_id");
    }

    /**
     * @param {object} [options] Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @param {string} [options.from] ISO date<br>
     * @param {string} [options.to] ISO date<br>
     * @param {string} [options.containerId] container ID<br>
     *     Only data within this range will be returned.
     * @returns Array of the retrieved records
     */
    public GetAll = async (
        options: {
            limit?: number;
            offset?: number;
            containerId?: string;
            knskoId?: string;
            from?: string;
            to?: string;
        } = {}
    ): Promise<any> => {
        const { limit, offset, from, to, containerId, knskoId } = options;
        try {
            let containerContainerObj: any[];

            if (knskoId)
                containerContainerObj = await sequelizeConnection?.query(
                    `
                SELECT sensoneo_code, id
                FROM public.containers_containers
                WHERE knsko_id='${knskoId}';
                `
                );
            else
                containerContainerObj = await sequelizeConnection?.query(
                    `
                SELECT sensoneo_code, id
                FROM public.containers_containers
                WHERE id='${containerId}';
                `
                );

            let sensoneoCode: string | null;
            let id: string;
            if (containerContainerObj[0][0]) {
                sensoneoCode = containerContainerObj[0][0].sensoneo_code;
                id = containerContainerObj[0][0].id;
            } else return [];
            if (!sensoneoCode) return [];
            const measurementContainerId = getUuid(sensoneoCode);

            const and: symbol = Sequelize.Op.and;
            const order: any[] = [];
            const where: any = {
                [and]: [],
            };

            where[and].push({ container_id: measurementContainerId });

            if (from) {
                where[and].push({
                    measured_at_utc: {
                        [Sequelize.Op.gte]: from,
                    },
                });
            }

            if (to) {
                where[and].push({
                    measured_at_utc: {
                        [Sequelize.Op.lte]: to,
                    },
                });
            }

            const attributes: string[] = [
                "container_code",
                "percent_calculated",
                "upturned",
                "temperature",
                "battery_status",
                "measured_at_utc",
                "prediction_utc",
                "firealarm",
                "updated_at",
                "created_at",
            ];

            order.push(["measured_at_utc", "DESC"]);

            const data = await this.sequelizeModel.findAll({
                attributes,
                limit,
                offset,
                order,
                raw: true,
                where,
            });

            return data.map((container: any) => {
                return {
                    id,
                    sensor_code: container.container_code,
                    percent_calculated: +container.percent_calculated || 0,
                    upturned: +container.upturned || 0,
                    temperature: +container.temperature || 0,
                    battery_status: +container.battery_status || 0,
                    measured_at_utc: container.measured_at_utc,
                    prediction_utc: container.prediction_utc,
                    firealarm: +container.firealarm || 0,
                    updated_at: container.updated_at
                        ? new Date(container.updated_at).getTime()
                        : container.created_at
                        ? new Date(container.created_at).getTime()
                        : 0,
                };
            });
        } catch (err) {
            throw new CustomError("Database error", true, "SortedWasteMeasurementsModel", 500, err);
        }
    };

    public GetOne = async (id: number): Promise<object | null> => {
        return null;
    };
}
