import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { sequelizeConnection } from "@golemio/core/dist/output-gateway/database";

export interface IPickDatesFromDb {
    knsko_id: number;
    sensoneo_code: string;
    pick_date: string;
}

export interface IPickDatesOutput {
    ksnko_id: number;
    sensoneo_code: string;
    generated_dates: string[];
}

export class SortedWastePickDatesModel {
    public constructor() {
        // nothing
    }

    public async GetAll(options?: { sensoneoCode?: string; knskoId?: string }): Promise<IPickDatesOutput[]> {
        try {
            let where = "where cc.sensoneo_code is not null and cc.sensoneo_code != cc.code ";

            if (options?.sensoneoCode) {
                where += ` and cc.sensoneo_code='${options.sensoneoCode}'`;
            }

            if (options?.knskoId) {
                where += ` and cc.knsko_id=${options.knskoId}`;
            }

            const data: any = await sequelizeConnection?.query(
                `
                select 
                cc.knsko_id,
                cc.sensoneo_code ,
                cpd.pick_date::date 
                from containers_picks_dates cpd
                left join containers_containers cc on cpd.container_id = cc.id
                ${where}
                order by knsko_id
                `
            );
            const dataArr: IPickDatesFromDb[] = data[0];
            let resultsArr: IPickDatesOutput[] = [];

            for (const item of dataArr) {
                let found = false;
                for (const resItem of resultsArr) {
                    if (item.knsko_id == resItem.ksnko_id) {
                        resItem.generated_dates.push(item.pick_date);
                        found = true;
                    }
                }
                if (!found) {
                    resultsArr.push({
                        ksnko_id: item.knsko_id,
                        sensoneo_code: item.sensoneo_code,
                        generated_dates: [item.pick_date],
                    });
                }
            }

            return resultsArr;
        } catch (err) {
            throw new CustomError("Database error", true, "SortedWastePickDatesModel", 500, err);
        }
    }
}
