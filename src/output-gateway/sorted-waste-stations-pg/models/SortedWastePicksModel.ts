import Sequelize from "@golemio/core/dist/shared/sequelize";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { SortedWasteStations } from "#sch/index";
import { sequelizeConnection } from "@golemio/core/dist/output-gateway/database";
import getUuid from "uuid-by-string";

export class SortedWastePicksModel extends SequelizeModel {
    public constructor() {
        super(
            SortedWasteStations.sensorsPicks.name,
            SortedWasteStations.sensorsPicks.pgTableName,
            SortedWasteStations.sensorsPicks.outputSequelizeAttributes
        );

        this.sequelizeModel.removeAttribute("vendor_id");
        this.sequelizeModel.removeAttribute("update_batch_id");
        this.sequelizeModel.removeAttribute("create_batch_id");
    }

    /**
     * @param {object} [options] Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @param {string} [options.from] ISO date<br>
     * @param {string} [options.to] ISO date<br>
     * @param {string} [options.containerId] container ID<br>
     *     Only data within this range will be returned.
     * @returns Array of the retrieved records
     */
    public GetAll = async (
        options: {
            limit?: number;
            offset?: number;
            containerId?: string;
            knskoId?: string;
            from?: string;
            to?: string;
        } = {}
    ): Promise<any> => {
        const { limit, offset, from, to, containerId, knskoId } = options;
        try {
            let containerContainerObj: any[];

            if (knskoId)
                containerContainerObj = await sequelizeConnection?.query(
                    `
                SELECT sensoneo_code, id
                FROM public.containers_containers
                WHERE knsko_id='${knskoId}';
                `
                );
            else
                containerContainerObj = await sequelizeConnection?.query(
                    `
                SELECT sensoneo_code, id
                FROM public.containers_containers
                WHERE id='${containerId}';
                `
                );

            let sensoneoCode: string | null;
            let id: string;
            if (containerContainerObj[0][0]) {
                sensoneoCode = containerContainerObj[0][0].sensoneo_code;
                id = containerContainerObj[0][0].id;
            } else return [];
            if (!sensoneoCode) return [];
            const measurementContainerId = getUuid(sensoneoCode);

            const and: symbol = Sequelize.Op.and;
            const order: any[] = [];
            const where: any = {
                [and]: [],
            };

            where[and].push({ container_id: measurementContainerId });

            if (from) {
                where[and].push({
                    pick_at_utc: {
                        [Sequelize.Op.gte]: from,
                    },
                });
            }

            if (to) {
                where[and].push({
                    pick_at_utc: {
                        [Sequelize.Op.lte]: to,
                    },
                });
            }

            const attributes: string[] = [
                "container_code",
                "pick_minfilllevel",
                "decrease",
                "pick_at_utc",
                "percent_before",
                "percent_now",
                "event_driven",
                "updated_at",
                "created_at",
            ];

            order.push(["pick_at_utc", "DESC"]);

            const data = await this.sequelizeModel.findAll({
                attributes,
                limit,
                offset,
                order,
                raw: true,
                where,
            });

            return data.map((container: any) => {
                return {
                    id,
                    sensor_code: container.container_code,
                    pick_minfilllevel: +container.pick_minfilllevel || 0,
                    decrease: +container.decrease || 0,
                    pick_at_utc: container.pick_at_utc,
                    percent_now: +container.percent_now || 0,
                    percent_before: +container.percent_before || 0,
                    event_driven: container.event_driven,
                    updated_at: container.updated_at
                        ? new Date(container.updated_at).getTime()
                        : container.created_at
                        ? new Date(container.created_at).getTime()
                        : 0,
                };
            });
        } catch (err) {
            throw new CustomError("Database error", true, "SortedWasteMeasurementsModel", 500, err);
        }
    };

    public GetOne = async (id: number): Promise<object | null> => {
        return null;
    };
}
