import { toNumber } from "lodash";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { query, oneOf } from "@golemio/core/dist/shared/express-validator";
import { parseCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { useCacheMiddleware } from "@golemio/core/dist/output-gateway/redis";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { checkErrors, paginationLimitMiddleware, pagination } from "@golemio/core/dist/output-gateway/Validation";
import {
    SortedWasteMeasurementsModel,
    SortedWastePicksModel,
    SortedWasteStationsModelPg,
    SortedWastePickDatesModel,
} from "./models";

export class SortedWasteRouterPg extends BaseRouter {
    protected stationsModel: SortedWasteStationsModelPg;
    protected measurementsModel: SortedWasteMeasurementsModel = new SortedWasteMeasurementsModel();
    protected picksModel: SortedWastePicksModel = new SortedWastePicksModel();
    protected pickDaysModel: SortedWastePickDatesModel = new SortedWastePickDatesModel();

    public constructor() {
        super();
        this.stationsModel = new SortedWasteStationsModelPg();

        this.router.get(
            "/measurements",
            [
                oneOf(
                    // <-- one of the following must exist
                    [query("containerId").exists().isString(), query("knskoId").exists().isString()]
                ),
                query("from").optional().isISO8601(),
                query("to").optional().isISO8601(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SortedWasteRouter"),
            useCacheMiddleware(),
            this.GetMeasurements
        );
        this.router.get(
            "/picks",
            [
                oneOf(
                    // <-- one of the following must exist
                    [query("containerId").exists().isString(), query("knskoId").exists().isString()]
                ),
                query("from").optional().isISO8601(),
                query("to").optional().isISO8601(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SortedWasteRouter"),
            useCacheMiddleware(),
            this.GetPicks
        );

        this.router.get(
            "/pickdays",
            [query("sensoneoCode").optional().isString(), query("knskoId").optional().isString()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SortedWasteRouter"),
            useCacheMiddleware(),
            this.GetPickDays
        );

        this.router.get(
            "/",
            [
                query("accessibility").optional().isNumeric(),
                query("onlyMonitored").optional().isBoolean(),
                query("districts").optional(),
                query("districts.*").isString(),
                // query("ids").optional(),
                query("latlng").optional().isString(),
                query("range").optional().isNumeric(),
                query("id").optional().isString(),
                query("knskoId").optional().isString(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SortedWasteRouter"),
            useCacheMiddleware(),
            this.GetAll
        );
    }

    public GetAll = async (req: Request, res: Response, next: NextFunction) => {
        let districts: any = req.query.districts;

        if (districts && !(districts instanceof Array)) {
            districts = districts.split(",");
        }

        try {
            const coords = await parseCoordinates(req.query.latlng as string, req.query.range as string);

            const data = await this.stationsModel.GetAll({
                accessibility: req.query.accessibility ? toNumber(req.query.accessibility) : undefined,
                districts,
                id: req.query.id as string,
                knskoId: req.query.knskoId as string,
                lat: coords.lat,
                limit: req.query.limit ? toNumber(req.query.limit) : undefined,
                lng: coords.lng,
                offset: req.query.offset ? toNumber(req.query.offset) : undefined,
                onlyMonitored: req.query.onlyMonitored as string,
                range: coords.range,
            });

            if (!data?.features[0]) return res.status(404).send();
            else res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetMeasurements = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.measurementsModel.GetAll({
                containerId: req.query.containerId as string,
                knskoId: req.query.knskoId as string,
                from: req.query.from as string,
                limit: req.query.limit ? toNumber(req.query.limit) : undefined,
                offset: req.query.offset ? toNumber(req.query.offset) : undefined,
                to: req.query.to as string,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetPicks = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.picksModel.GetAll({
                containerId: req.query.containerId as string,
                knskoId: req.query.knskoId as string,
                from: req.query.from as string,
                limit: req.query.limit ? toNumber(req.query.limit) : undefined,
                offset: req.query.offset ? toNumber(req.query.offset) : undefined,
                to: req.query.to as string,
            });
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetPickDays = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.pickDaysModel.GetAll({
                sensoneoCode: req.query.sensoneoCode as string,
                knskoId: req.query.knskoId as string,
            });
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };
}

const sortedWasteRouterPg: Router = new SortedWasteRouterPg().router;

export { sortedWasteRouterPg };
