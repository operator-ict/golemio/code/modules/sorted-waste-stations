import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { config } from "@golemio/core/dist/integration-engine/config";
import { SortedWasteStations } from "#sch/index";
import { SortedWasteStationsWorker } from "#ie/SortedWasteStationsWorker";

describe("SortedWasteStationsWorker", () => {
    let worker: SortedWasteStationsWorker;
    let sandbox: SinonSandbox;
    let queuePrefix: string;
    let testData: number[];
    let testTransformedData: any[];
    let testSensorContainersData: any[];
    let testSensorMeasurementData: any[];
    let testSensorPicksData: any[];

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        testData = [1, 2];
        testTransformedData = [
            {
                geometry: { coordinates: [0, 0] },
                properties: {
                    accessibility: { id: 1 },
                    containers: [
                        {
                            sensor_code: "0006/ 263C00231",
                            sensor_container_id: 29823,
                            sensor_supplier: "Sensoneo",
                            trash_type: { id: 6 },
                        },
                    ],
                    id: 10,
                },
                remove: sandbox.stub().resolves(true),
                save: sandbox.stub().resolves(true),
            },
            {
                geometry: { coordinates: [1, 1] },
                properties: { accessibility: { id: 1 }, containers: [{}], id: "diakonie-broumov_zahradnickova" },
                remove: sandbox.stub().resolves(true),
                save: sandbox.stub().resolves(true),
            },
            {
                geometry: { coordinates: [2, 2] },
                properties: { accessibility: { id: 1 }, containers: [{}], id: "potex-dolni-pocernice-2" },
                save: sandbox.stub().resolves(true),
            },
        ];
        testSensorContainersData = [
            {
                address: "Nad Úžlabinou 708",
                bin_type: "Schäfer/Europa-OV",
                code: "0010/ 268C00462",
                id: 29823,
                latitude: 50.08035899999999,
                longitude: 14.49945600000001,
                trash_type: "plastic",
            },
        ];
        testSensorMeasurementData = [
            {
                battery_status: 3.6,
                code: "0006/ 263C00231",
                container_id: 29823,
                firealarm: 0,
                id: 12536378,
                measured_at_utc: "1557806982000",
                percent_calculated: 56,
                prediction_utc: "1557806982000",
                temperature: 10,
                updated_at: 1559737670311,
                upturned: 0,
            },
        ];
        testSensorPicksData = [
            {
                code: "0001/ 038C00042",
                container_id: 29823,
                decrease: 20,
                event_driven: false,
                id: 12495474,
                percent_before: 100,
                percent_now: 10,
                pick_at_utc: "1557806982000",
                pick_minfilllevel: 30,
                updated_at: 1559737670311,
            },
        ];

        worker = new SortedWasteStationsWorker();

        sandbox.stub(worker["iprContainersDatasource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(worker["iprStationsDatasource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(worker["oictDatasource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(worker["potexDatasource"], "getAll").callsFake(() => Promise.resolve(testData));

        sandbox.stub(worker["iprTransformation"], "transform").callsFake(() => testTransformedData[0]);
        sandbox.stub(worker["iprTransformation"], "setContainers");
        sandbox.stub(worker["oictTransformation"], "transform").callsFake(() => Promise.resolve([testTransformedData[1]]));
        sandbox.stub(worker["potexTransformation"], "transform").callsFake(() => Promise.resolve([testTransformedData[2]]));

        sandbox.stub(worker["model"], "save");
        sandbox.stub(worker, "sendMessageToExchange" as any);
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + SortedWasteStations.name.toLowerCase();
        sandbox.stub(worker["model"], "findOneById").callsFake(() => testTransformedData[1]);
        sandbox.stub(worker["model"], "findOne").callsFake(() => testTransformedData[0]);
        sandbox
            .stub(worker, "mergeContainersIntoStations" as any)
            .callsFake(() => [[testTransformedData[0]], [testTransformedData[1], testTransformedData[2]]]);
        sandbox.stub(worker["model"], "updateOne");
        sandbox.stub(worker, "pairSensorWithContainer" as any);
        sandbox.stub(worker["model"], "aggregate");

        sandbox.stub(worker["sensorsContainersDatasource"], "getAll").callsFake(() => Promise.resolve(testSensorContainersData));
        sandbox
            .stub(worker["sensorsMeasurementsDatasource"], "getAll")
            .callsFake(() => Promise.resolve(testSensorMeasurementData));
        sandbox.stub(worker["sensorsMeasurementsModel"], "save");
        sandbox.stub(worker["sensorsMeasurementsModel"], "aggregate").callsFake(() => Promise.resolve([]));
        sandbox.stub(worker["sensorsPicksDatasource"], "getAll").callsFake(() => Promise.resolve(testSensorPicksData));
        sandbox.stub(worker["sensorsPicksModel"], "save");
        sandbox.stub(worker["sensorsPicksModel"], "aggregate").callsFake(() => Promise.resolve([]));

        sandbox
            .stub(worker["sensoneoMeasurementsTransformation"], "transform")
            .callsFake(() => Promise.resolve(testSensorMeasurementData));
        sandbox.stub(worker["sensoneoPicksTransformation"], "transform").callsFake(() => Promise.resolve(testSensorPicksData));

        sandbox.stub(worker["cityDistrictsModel"], "findOne").callsFake(() => Object.assign({ properties: { slug: "praha-1" } }));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by refreshDataInDB method", async () => {
        await worker.refreshDataInDB({});
        sandbox.assert.calledOnce(worker["iprContainersDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["iprStationsDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["oictDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["potexDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["iprTransformation"].setContainers as SinonSpy);
        sandbox.assert.calledOnce(worker["iprTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["oictTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["potexTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["model"].save as SinonSpy);
        testTransformedData.map((f) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + ".updateDistrict",
                JSON.stringify(f)
            );
        });
        sandbox.assert.callOrder(
            worker["iprContainersDatasource"].getAll as SinonSpy,
            worker["iprTransformation"].setContainers as SinonSpy,
            worker["iprStationsDatasource"].getAll as SinonSpy,
            worker["iprTransformation"].transform as SinonSpy,
            worker["oictDatasource"].getAll as SinonSpy,
            worker["oictTransformation"].transform as SinonSpy,
            worker["potexDatasource"].getAll as SinonSpy,
            worker["potexTransformation"].transform as SinonSpy,
            worker["model"].save as SinonSpy,
            worker["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by updateDistrict method (different geo)", async () => {
        await worker.updateDistrict({ content: Buffer.from(JSON.stringify(testTransformedData[0])) });
        sandbox.assert.calledOnce(worker["model"].findOneById as SinonSpy);
        sandbox.assert.calledWith(worker["model"].findOneById as SinonSpy, testTransformedData[0].properties.id);

        sandbox.assert.calledOnce(worker["cityDistrictsModel"].findOne as SinonSpy);
        sandbox.assert.calledOnce(testTransformedData[1].save);
        sandbox.assert.notCalled(testTransformedData[1].remove);
    });

    it("should calls the correct methods by updateDistrict method (same geo)", async () => {
        testTransformedData[1] = {
            geometry: { coordinates: [0, 0] },
            properties: {
                address: "a",
                district: "praha-0",
                id: 1,
            },
            remove: sandbox.stub().resolves(true),
            save: sandbox.stub().resolves(true),
        };
        await worker.updateDistrict({ content: Buffer.from(JSON.stringify(testTransformedData[0])) });
        sandbox.assert.calledOnce(worker["model"].findOneById as SinonSpy);
        sandbox.assert.calledWith(worker["model"].findOneById as SinonSpy, testTransformedData[0].properties.id);

        sandbox.assert.notCalled(worker["cityDistrictsModel"].findOne as SinonSpy);
        sandbox.assert.notCalled(testTransformedData[1].save);
        sandbox.assert.notCalled(testTransformedData[1].remove);
    });

    it("should calls the correct methods by getSensorsAndPairThemWithContainers method", async () => {
        await worker.getSensorsAndPairThemWithContainers({});

        sandbox.assert.calledOnce(worker["sensorsContainersDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["pairSensorWithContainer"] as SinonSpy);
    });

    it("should calls the correct methods by updateSensorsMeasurement method", async () => {
        await worker.updateSensorsMeasurement({});

        sandbox.assert.calledOnce(worker["sensorsMeasurementsDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["sensorsMeasurementsModel"].save as SinonSpy);
        testSensorMeasurementData.map((f) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + ".updateSensorsMeasurementInContainer",
                JSON.stringify(f)
            );
        });
    });

    it("should calls the correct methods by updateSensorsMeasurementInContainer method", async () => {
        await worker.updateSensorsMeasurementInContainer({ content: Buffer.from(JSON.stringify(testSensorMeasurementData[0])) });
        sandbox.assert.calledOnce(worker["model"].findOne as SinonSpy);
        sandbox.assert.calledOnce(worker["model"].updateOne as SinonSpy);
    });

    it("should calls the correct methods by updateSensorsPicks method", async () => {
        await worker.updateSensorsPicks({});

        sandbox.assert.calledOnce(worker["sensorsPicksDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["sensorsPicksModel"].save as SinonSpy);
        testSensorPicksData.map((f) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + ".updateSensorsPicksInContainer",
                JSON.stringify(f)
            );
        });
    });

    it("should calls the correct methods by updateSensorsPicksInContainer method", async () => {
        await worker.updateSensorsPicksInContainer({ content: Buffer.from(JSON.stringify(testSensorPicksData[0])) });
        sandbox.assert.calledOnce(worker["model"].findOne as SinonSpy);
        sandbox.assert.calledOnce(worker["model"].updateOne as SinonSpy);
    });
});
