import axios from "@golemio/core/dist/shared/axios";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { KsnkoNotifier } from "#ie/helpers/KsnkoNotifier";

describe("KsnkoNotifier", () => {
    let notifier: KsnkoNotifier;
    let sandbox: SinonSandbox;

    const changedContainers = {
        attached: [
            {
                knsko_id: 1551,
                sensor_id: 111111,
            },
        ],
        detached: [
            {
                knsko_id: 1551,
                sensor_id: 222222,
            },
        ],
    };

    const reqDataToDb = {
        request:
            // eslint-disable-next-line max-len
            '{"data":{"sensorId":222222},"headers":{"Authorization":"Bearer ***","Content-Type":"application/json","accept":"application/json"},"method":"POST","url":"<ksnkoApiUrl>/station-item-containers/1551/sensor/unassign"}',
        knsko_id: 1551,
        sensor_id: 222222,
        mode: "unassign",
    };

    const respDataToDb = {
        id: 12,
        success: true,
        response: '{"status":200,"statusText":"Ok"}',
    };

    const respDataWithErrorToDb = {
        id: 12,
        success: false,
        response: '{"error":"Server Error"}',
    };

    const respDataWithAxiosErrorToDb = {
        id: 12,
        success: false,
        response: '{"status":400,"statusText":"Bad Request","headers":{},"data":{"message":"Sensor is already unassigned"}}',
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        sandbox.stub(config, "datasources").value({
            KSNKOApi: {
                url: "<ksnkoApiUrl>",
            },
            SortedWastePicks: {},
        });
        notifier = new KsnkoNotifier();

        sandbox.stub(notifier["ksnkoNotificationsInsertModel"], "save").resolves({
            id: 12,
        });
        sandbox.stub(notifier["ksnkoNotificationsUpdateModel"], "save");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by notifyChangedSensors method", async () => {
        sandbox.stub(axios, "request").resolves({
            status: 200,
            statusText: "Ok",
        });
        await notifier.notifyChangedSensors(changedContainers, "token");
        sandbox.assert.callCount(axios.request as SinonSpy, 2);
        sandbox.assert.callCount(notifier["ksnkoNotificationsInsertModel"]["save"] as SinonSpy, 2);
        sandbox.assert.callCount(notifier["ksnkoNotificationsUpdateModel"]["save"] as SinonSpy, 2);
    });

    it("should save to DB correct request and response data", async () => {
        sandbox.stub(axios, "request").resolves({
            status: 200,
            statusText: "Ok",
        });
        await notifier.notifyChangedSensors(changedContainers, "token");
        sandbox.assert.calledWith(notifier["ksnkoNotificationsInsertModel"]["save"] as SinonSpy, reqDataToDb);
        sandbox.assert.calledWith(notifier["ksnkoNotificationsUpdateModel"]["save"] as SinonSpy, respDataToDb);
    });

    it("should save to DB correct request and response data with server error", async () => {
        sandbox.stub(axios, "request").rejects({
            message: "Server Error",
        });
        await notifier.notifyChangedSensors(changedContainers, "token");
        sandbox.assert.calledWith(notifier["ksnkoNotificationsInsertModel"]["save"] as SinonSpy, reqDataToDb);
        sandbox.assert.calledWith(notifier["ksnkoNotificationsUpdateModel"]["save"] as SinonSpy, respDataWithErrorToDb);
    });

    it("should save to DB correct response with error from KSNKO", async () => {
        sandbox.stub(axios, "request").rejects({
            message: "Bad Request",
            response: {
                status: 400,
                statusText: "Bad Request",
                headers: {},
                data: {
                    message: "Sensor is already unassigned",
                },
            },
            isAxiosError: true,
        });
        await notifier.notifyChangedSensors(changedContainers, "token");
        sandbox.assert.calledWith(notifier["ksnkoNotificationsUpdateModel"]["save"] as SinonSpy, respDataWithAxiosErrorToDb);
    });
});
