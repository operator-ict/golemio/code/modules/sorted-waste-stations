// Portman main process configuration:

export const portmanMainConfig = {
    // url of testing server
    baseUrl: "http://localhost:3011",

    // swagger doc location
    oaLocal: "docs/openapi.yaml",

    // postman collection fransformed from swagger destination
    oaOutput: "docs/postmanCollectionGenerated.json",

    // tests definitions, owerwraiting, skipping
    portmanConfigPath: "test/api-docs/portmanConfig/portman-config.json",

    // swagger transformation config (docs: https://github.com/postmanlabs/openapi-to-postman/blob/develop/OPTIONS.md)
    postmanConfigPath: "test/api-docs/portmanConfig/postman-config.json",

    // filter methods, tags, operations, flags...
    filterFile: "test/api-docs/portmanConfig/filter-config.json",

    // add or not tests to swager transformation
    includeTests: true,

    // run or not the tests
    runNewman: true,

    // not in use for current tests task:
    syncPostman: false,
    _: [],
    $0: "",
    oaUrl: "",
    bundleContractTests: false,
    newmanIterationData: "",
    portmanConfigFile: "",
    postmanConfigFile: "",
    envFile: ".env",
};
