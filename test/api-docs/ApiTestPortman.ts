const PortmanPackage = require("@apideck/portman/dist/Portman");

interface IPortmanOptions {
    _: never[];
    $0: string;
    oaUrl: string;
    oaLocal: string;
    baseUrl: string;
    includeTests: boolean;
    bundleContractTests: boolean;
    runNewman: boolean;
    newmanIterationData: string;
    syncPostman: boolean;
    portmanConfigFile: string;
    portmanConfigPath: string;
    postmanConfigFile: string;
    postmanConfigPath: string;
    envFile: string | null;
    filterFile: string | null;
    oaOutput: string;
}

export const ApiTestPortman = async (portmanOptions: IPortmanOptions): Promise<void> => {
    const portmanProcess = new PortmanPackage.Portman(portmanOptions);
    await portmanProcess.run();
};
