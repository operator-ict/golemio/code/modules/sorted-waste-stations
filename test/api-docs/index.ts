// Load reflection lib
import "@golemio/core/dist/shared/_global";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import ApiTestServer from "./ApiTestServer";
import { ApiTestPortman } from "./ApiTestPortman";
import { portmanMainConfig } from "./portmanConfig/portmanMainConfig";

const server = new ApiTestServer();

async function apitesting() {
    await server.start();
    log.info(`Starting Portman process`);
    await ApiTestPortman(portmanMainConfig);
    await server.stop();
    process.exit(0);
}

apitesting();
