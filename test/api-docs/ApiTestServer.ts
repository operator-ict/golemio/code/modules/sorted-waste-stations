import { CustomError, ErrorHandler, HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import http from "http";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { sequelizeConnection } from "@golemio/core/dist/output-gateway/database";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { sortedWasteRouterPg } from "#og/sorted-waste-stations-pg/SortedWasteRouter";

/**
 * Entry point of the api docs testing application. Creates and configures an ExpressJS web server, running Portman.
 */
export default class ApiTestServer {
    // Create a new express application instance
    public express: express.Application = express();
    // The port the express app will listen on
    public port: number = 3011;
    private server?: http.Server;

    // Starts the application and runs the server
    public start = async (): Promise<void> => {
        try {
            this.express = express();
            await this.database();
            this.middleware();
            this.routes();
            this.errorHandlers();

            return new Promise((resolve, reject) => {
                this.server = http.createServer(this.express);
                // Setup error handler hook on server error
                this.server.on("error", (err: Error) => {
                    ErrorHandler.handle(new CustomError("Could not start a server", false, "App", 1, err), log);
                });
                // Serve the application at the given port
                this.server.listen(this.port, () => {
                    // Success callback
                    log.info(`Listening at http://localhost:${this.port}/`);
                    resolve();
                });
            });
        } catch (err) {
            ErrorHandler.handle(err, log);
        }
    };

    /**
     * terminate connections and server
     */
    public stop = async (): Promise<void> => {
        await sequelizeConnection.close();
        this.server?.close();
    };

    private setHeaders = (req: Request, res: Response, next: NextFunction): void => {
        res.setHeader("x-powered-by", "shem");
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS, HEAD");
        next();
    };

    private database = async (): Promise<void> => {
        await sequelizeConnection?.authenticate();
    };

    private middleware = (): void => {
        this.express.use(this.setHeaders);
        this.express.use(express.static("public"));
    };

    private routes = (): void => {
        this.express.use("/sortedwastestations", sortedWasteRouterPg);
    };

    private errorHandlers = (): void => {
        // Not found error - no route was matched
        this.express.use((req, res, next) => {
            next(new CustomError("Route not found", true, "ApiTestServer", 404, new Error(`Called ${req.method} ${req.url}`)));
        });

        // Error handler to catch all errors sent by routers (propagated through next(err))
        this.express.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const warnCodes = [400, 404];
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(
                err,
                log,
                warnCodes.includes(err.code) ? "warn" : "error"
            );
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    };
}
