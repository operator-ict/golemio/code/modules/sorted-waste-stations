import sinon from "sinon";
import request from "supertest";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import { HTTPErrorHandler, ICustomErrorObject } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { sortedWasteRouterPg } from "#og/sorted-waste-stations-pg/SortedWasteRouter";

chai.use(chaiAsPromised);

describe("SortedWaste Router", () => {
    // Create clean express instance
    const app = express();
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        // Mount the tested router to the express instance
        app.use("/sortedwastestationspg", sortedWasteRouterPg);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: ICustomErrorObject = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /sortedwastestations", (done) => {
        request(app)
            .get("/sortedwastestationspg?limit=10")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });

    it("should respond with json to GET /sortedwastestations/measurements", (done) => {
        request(app)
            .get("/sortedwastestationspg/measurements?containerId=7089c1ba-c2a4-52e9-bd54-9448bfdd7afe")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });

    it("should respond with json to GET /sortedwastestations/picks", (done) => {
        request(app)
            .get("/sortedwastestationspg/picks?containerId=7089c1ba-c2a4-52e9-bd54-9448bfdd7afe")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });

    it("should respond with json to GET /sortedwastestations/pickdays", (done) => {
        request(app)
            .get("/sortedwastestationspg/pickdays")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });
});
